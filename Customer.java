package model;

public class Customer {
	
	public int idCustomer;
	public String name1;
	public String adress;
	public String email;
	public String telephone;
	
	public Customer(int id,String name,String adress,String email, String telephone) 
	{
		this.idCustomer=id;
		this.name1=name;
		this.adress=adress;
		this.email=email;
		this.telephone=telephone;
	}
	public Customer() 
	{
		
	}

	public int getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(int idCustomer) {
		this.idCustomer = idCustomer;
	}

	public String getName1() {
		return name1;
	}

	public void setName1(String name) {
		this.name1 = name;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String toString() 
	{
		return "Id: "+this. idCustomer +" Name: "+this.name1+" Adress: "+this.adress+" Email: "+this.email+" Phone NR: "+this.telephone;
		
	}
	

}
