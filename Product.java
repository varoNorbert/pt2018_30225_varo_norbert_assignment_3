package model;

public class Product {
	public int idProduct;
	public String name1;
	public int price;
	public int stock;
	
	public Product(int id,String name1, int price, int stock) 
	{
		this.idProduct=id;
		this.name1=name1;
		this.price=price;
		this.stock=stock;
	}
	
	public Product() 
	{
		
	}
	
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getName1() {
		return name1;
	}
	public void setName1(String name1) {
		this.name1 = name1;
	}
	public int getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}

	public void updateStock(int parseInt) {
		// TODO Auto-generated method stub
		this.stock-=parseInt;
	}

}
