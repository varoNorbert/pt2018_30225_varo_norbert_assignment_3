package gui;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.lang.reflect.Field;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import DAO.CustomerDAO;
import DAO.OrderDAO;
import DAO.ProductDAO;
import bll.Validator;
import model.Customer;
import model.Order;
import model.Product;

public class GraphicInterface extends JFrame implements ActionListener{
	
	private JPanel contentPane;
	
	JButton addCustomer=new JButton("Add new client");
	JButton removeCustomer=new JButton("Remove existing client");
	JButton updateCustomer=new JButton("Update existing client");
	JButton displayCustomer=new JButton("Display Clients");
	JButton addProduct=new JButton("Add new product");
	JButton updateProduct=new JButton("Update product");
	JButton displayProduct=new JButton("Display Products");
	JButton addOrder=new JButton("Make Order");
	JButton cancelOrder=new JButton("Cancel Order");
	
	JButton displayOrder=new JButton("Display Orders");
	JButton removeProduct=new JButton("Remove product");
	JTable table = new JTable();
	JButton submit=new JButton("Submit");
	ArrayList<JTextField> inputs=new ArrayList<JTextField>();
	JButton submitProduct=new JButton("Submit Product");
	JButton submitOrder=new JButton("Make Order");
	JButton submitDelete=new JButton("Delete Client");
	JButton submitDeleteProduct=new JButton("Delete Product");
	JButton submitUpdateProduct=new JButton("Update Product");
	JButton submitUpdateClient=new JButton("Update Client Info");

	JButton cancelSubmit=new JButton("Cancel Order");

	
	public GraphicInterface() 
	{
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		setTitle("Shopping is fun");
		
		contentPane=new JPanel();
		
		contentPane.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		c.insets = new Insets(10,10,10,10);
		//c.anchor=GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.HORIZONTAL;
		
		
		c.gridx = 0;
		c.gridy = 0;
		//c.anchor=GridBagConstraints.CENTER;
		c.gridwidth = 2;
		
		c.weightx = 0.5;
		
		JLabel top=new JLabel("Hello! Make your choice:");
		top.setForeground(Color.BLACK);
		top.setFont(new Font("SansSerif", Font.BOLD, 20));
		contentPane.add(top,c);
	
		c.gridwidth = 1;
		
		
		
		c.gridx = 0;
		c.gridy = 1;
		
		addCustomer.setPreferredSize(new Dimension(170,30));
		addCustomer.setBackground(Color.BLACK);
		addCustomer.setForeground(Color.WHITE);
		addCustomer.addActionListener(this);
		addCustomer.setFocusPainted(false);
		contentPane.add(addCustomer,c);
		
		c.gridx = 1;
		c.gridy = 1;
		
		
		removeCustomer.setPreferredSize(new Dimension(170,30));
		removeCustomer.setBackground(Color.BLACK);
		removeCustomer.setForeground(Color.WHITE);
		removeCustomer.addActionListener(this);
		removeCustomer.setFocusPainted(false);
		contentPane.add(removeCustomer,c);
		
		c.gridx = 2;
		c.gridy = 1;
		
		
		updateCustomer.setPreferredSize(new Dimension(170,30));
		updateCustomer.setBackground(Color.BLACK);
		updateCustomer.setForeground(Color.WHITE);
		updateCustomer.addActionListener(this);
		updateCustomer.setFocusPainted(false);
		contentPane.add(updateCustomer,c);
		
		c.gridx = 3;
		c.gridy = 1;
		
		
		displayCustomer.setPreferredSize(new Dimension(170,30));
		displayCustomer.setBackground(Color.BLACK);
		displayCustomer.setForeground(Color.WHITE);
		displayCustomer.addActionListener(this);
		displayCustomer.setFocusPainted(false);
		contentPane.add(displayCustomer,c);
		
		
		
		c.gridx = 0;
		c.gridy = 2;
		
		
		addProduct.setPreferredSize(new Dimension(170,30));
		addProduct.setBackground(Color.DARK_GRAY);
		addProduct.setForeground(Color.WHITE);
		addProduct.addActionListener(this);
		addProduct.setFocusPainted(false);
		contentPane.add(addProduct,c);
		
		c.gridx = 1;
		c.gridy = 2;
		
	
		removeProduct.setPreferredSize(new Dimension(170,30));
		removeProduct.setBackground(Color.DARK_GRAY);
		removeProduct.setForeground(Color.WHITE);
		removeProduct.addActionListener(this);
		removeProduct.setFocusPainted(false);
		contentPane.add(removeProduct,c);
		
		c.gridx = 2;
		c.gridy = 2;
		
		
		updateProduct.setPreferredSize(new Dimension(170,30));
		updateProduct.setBackground(Color.DARK_GRAY);
		updateProduct.setForeground(Color.WHITE);
		updateProduct.addActionListener(this);
		updateProduct.setFocusPainted(false);
		contentPane.add(updateProduct,c);
		
		c.gridx = 3;
		c.gridy = 2;
		
		
		displayProduct.setPreferredSize(new Dimension(170,30));
		displayProduct.setBackground(Color.DARK_GRAY);
		displayProduct.setForeground(Color.WHITE);
		displayProduct.addActionListener(this);
		displayProduct.setFocusPainted(false);
		contentPane.add(displayProduct,c);
		
		
		c.gridx = 0;
		c.gridy = 3;
		
		
		addOrder.setPreferredSize(new Dimension(170,30));
		addOrder.setBackground(Color.GRAY);
		addOrder.setForeground(Color.WHITE);
		addOrder.addActionListener(this);
		addOrder.setFocusPainted(false);
		contentPane.add(addOrder,c);
		
		c.gridx = 1;
		c.gridy = 3;
		
		
		cancelOrder.setPreferredSize(new Dimension(170,30));
		cancelOrder.setPreferredSize(new Dimension(170,30));
		cancelOrder.setBackground(Color.GRAY);
		cancelOrder.setForeground(Color.WHITE);
		cancelOrder.addActionListener(this);
		contentPane.add(cancelOrder,c);
		
		c.gridx = 2;
		c.gridy = 3;
		
	
		
		c.gridx = 3;
		c.gridy = 3;
		
		
		displayOrder.setPreferredSize(new Dimension(170,30));
		displayOrder.setPreferredSize(new Dimension(170,30));
		displayOrder.setBackground(Color.GRAY);
		displayOrder.setForeground(Color.WHITE);
		displayOrder.addActionListener(this);
		contentPane.add(displayOrder,c);
	
		
		
		setContentPane(contentPane);
		getContentPane().setBackground( Color.GREEN);
		pack();
		setVisible(true);
		
		
	}
	
	public void clearBottom() 
	{
		contentPane.remove(submitOrder);
		contentPane.remove(submit);
		contentPane.remove(submitProduct);
		contentPane.remove(submitDelete);
		contentPane.remove(submitDeleteProduct);
		contentPane.remove(cancelSubmit);
		contentPane.remove(submitUpdateClient);
		contentPane.remove(submitUpdateProduct);
	
		for(int i=0;i<inputs.size();i++) {
		//s[i]=inputs.get(i).getText();
		contentPane.remove(inputs.get(i));	
		
		}
		inputs.removeAll(inputs);
		contentPane.remove(table.getTableHeader());
		contentPane.remove(table);
	}

	
	public void printTable(ArrayList<Order> toDisplay) 
	{
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(10,10,10,10);
		c.anchor=GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 4;
		c.gridwidth = 4;
		contentPane.add(table.getTableHeader(),c);
		c.gridy++;
		contentPane.add(table,c);
		String[] s=new String[4];
		int i=0;
		Field[] f=toDisplay.get(0).getClass().getDeclaredFields();
		for(Field fi:f) 
		{
			s[i]=fi.getName();
			i++;
		}
		
		DefaultTableModel model = new DefaultTableModel(s, 0);
	
		for(Order i1:toDisplay) 
		{
			model.addRow(new Object[] {Integer.toString(i1.getIdOrder()),Integer.toString(i1.getIdCustomer()),Integer.toString(i1.getIdProduct()),Integer.toString(i1.getQuantity())});
				
		}
		table.setModel(model);
	}
	
	public void printTableP(ArrayList<Product> toDisplay) 
	{
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(10,10,10,10);
		c.anchor=GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 4;
		c.gridwidth = 4;
		contentPane.add(table.getTableHeader(),c);
		c.gridy++;
		contentPane.add(table,c);
		String[] s=new String[4];
		int i=0;
		Field[] f=toDisplay.get(0).getClass().getDeclaredFields();
		for(Field fi:f) 
		{
			s[i]=fi.getName();
			i++;
		}
		
		DefaultTableModel model = new DefaultTableModel(s, 0);
	
		for(Product i1:toDisplay) 
		{
			model.addRow(new Object[] {Integer.toString(i1.getIdProduct()),i1.getName1(),Integer.toString(i1.getPrice()),Integer.toString(i1.getStock())});
				
		}
		table.setModel(model);
	}
	
	public void printTableC(ArrayList<Customer> toDisplay) 
	{
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(10,10,10,10);
		c.anchor=GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 4;
		c.gridwidth = 4;
		contentPane.add(table.getTableHeader(),c);
		c.gridy++;
		contentPane.add(table,c);
		String[] s=new String[5];
		int i=0;
		Field[] f=toDisplay.get(0).getClass().getDeclaredFields();
		for(Field fi:f) 
		{
			s[i]=fi.getName();
			i++;
		}
		
		DefaultTableModel model = new DefaultTableModel(s, 0);
	
		for(Customer i1:toDisplay) 
		{
			model.addRow(new Object[] {Integer.toString(i1.getIdCustomer()),i1.getName1(),i1.getAdress(),i1.getEmail(),i1.getTelephone()});

		}
		table.setModel(model);
	}
	
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(arg0.getSource()==updateCustomer) 
		{
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(10,10,10,10);
			c.anchor=GridBagConstraints.CENTER;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 4;
			c.gridwidth = 1;

			clearBottom();
			Customer toInsert=new Customer();
			
			Field[] fields=toInsert.getClass().getDeclaredFields();
			
			int i=0;
			for(Field f:fields) 
			{
				inputs.add(new JTextField(f.getName()));
				c.gridx=i;
				contentPane.add(inputs.get(i),c);
				i++;
			}
			
			c.gridx=2;
			c.gridy=5;
		
			submitUpdateClient.addActionListener(this);
			contentPane.add(submitUpdateClient,c);
			
			contentPane.revalidate();
			contentPane.repaint();
			
			pack();
			
			
			
		}
		if(arg0.getSource()==submitUpdateClient) 
		{
			
			String[] s=new String[5];
			
			for(int i=0;i<inputs.size();i++) {
			s[i]=inputs.get(i).getText();
			
			
			}
		
			clearBottom();
			CustomerDAO update=new CustomerDAO();
			int ok=2;
			try {
			ok=update.update(new Customer(Integer.parseInt(s[0]),s[1],s[2],s[3],s[4]));
			}
			catch(NumberFormatException e) 
			{
				
			}
			if(ok==0)
			JOptionPane.showMessageDialog(null, "The client has been Updated!");
			contentPane.revalidate();
			contentPane.repaint();
			pack();
		}
		
		
		if(arg0.getSource()==updateProduct) 
		{
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(10,10,10,10);
			c.anchor=GridBagConstraints.CENTER;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 4;
			c.gridwidth = 1;

			clearBottom();
			Product toInsert=new Product();
			
			Field[] fields=toInsert.getClass().getDeclaredFields();
			
			int i=0;
			for(Field f:fields) 
			{
				inputs.add(new JTextField(f.getName()));
				c.gridx=i;
				contentPane.add(inputs.get(i),c);
				i++;
			}
			
			c.gridx=2;
			c.gridy=5;
		
			submitUpdateProduct.addActionListener(this);
			contentPane.add(submitUpdateProduct,c);
			
			contentPane.revalidate();
			contentPane.repaint();
			
			pack();
			
			
			
		}
		if(arg0.getSource()==submitUpdateProduct) 
		{
			
			String[] s=new String[5];
			
			for(int i=0;i<inputs.size();i++) {
			s[i]=inputs.get(i).getText();
			
			
			}
		
			clearBottom();
			ProductDAO update=new ProductDAO();
			int ok=2;
			try {
			ok=update.update(new Product(Integer.parseInt(s[0]),s[1],Integer.parseInt(s[2]),Integer.parseInt(s[3])));
			}
			catch(NumberFormatException e) 
			{
				
			}
			if(ok==0)
			JOptionPane.showMessageDialog(null, "The product has been updated!");
			contentPane.revalidate();
			contentPane.repaint();
			pack();
		}
		
		
		
		if(arg0.getSource()==displayCustomer) 
		{
			CustomerDAO query=new CustomerDAO();
			ArrayList<Customer> toDisplay=(ArrayList<Customer>) query.returnAll();
			clearBottom();
			
			printTableC(toDisplay);
			contentPane.revalidate();
			contentPane.repaint();
			
			pack();
			
			
		}
		
		if(arg0.getSource()==displayProduct) 
		{
			ProductDAO query=new ProductDAO();
			ArrayList<Product> toDisplay=(ArrayList<Product>) query.returnAll();
			clearBottom();
			
			printTableP(toDisplay);
			contentPane.revalidate();
			contentPane.repaint();
		
			pack();
			
			
			
		}
		if(arg0.getSource()==displayOrder) 
		{
			OrderDAO query=new OrderDAO();
			ArrayList<Order> toDisplay=(ArrayList<Order>) query.returnAll();
			

			clearBottom();
		
			printTable(toDisplay);
			
			contentPane.revalidate();
			contentPane.repaint();
			
			pack();
			
			
			
		}
		
		if(arg0.getSource()==addCustomer) 
		{
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(10,10,10,10);
			c.anchor=GridBagConstraints.CENTER;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 4;
			c.gridwidth = 1;

			clearBottom();
			Customer toInsert=new Customer();
			
			Field[] fields=toInsert.getClass().getDeclaredFields();
			
			int i=0;
			for(Field f:fields) 
			{
				inputs.add(new JTextField(f.getName()));
				c.gridx=i;
				contentPane.add(inputs.get(i),c);
				i++;
			}
			
			c.gridx=2;
			c.gridy=5;
			submit.addActionListener(this);
			contentPane.add(submit,c);
			
			contentPane.revalidate();
			contentPane.repaint();
			
			pack();
			
			
			
		}
		if(arg0.getSource()==addProduct) 
		{
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(10,10,10,10);
			c.anchor=GridBagConstraints.CENTER;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 4;
			c.gridwidth = 1;

			clearBottom();
			Product toInsert=new Product();
			
			Field[] fields=toInsert.getClass().getDeclaredFields();
			
			int i=0;
			for(Field f:fields) 
			{
				inputs.add(new JTextField(f.getName()));
				c.gridx=i;
				contentPane.add(inputs.get(i),c);
				i++;
			}
			
			c.gridx=2;
			c.gridy=5;
		
			submitProduct.addActionListener(this);
			contentPane.add(submitProduct,c);
			
			contentPane.revalidate();
			contentPane.repaint();
			
			pack();
			
			
			
		}
		
		if(arg0.getSource()==addOrder) 
		{
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(10,10,10,10);
			c.anchor=GridBagConstraints.CENTER;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 4;
			c.gridwidth = 1;

			clearBottom();
			Order toInsert=new Order();
			
			Field[] fields=toInsert.getClass().getDeclaredFields();
			
			int i=0;
			for(Field f:fields) 
			{
				inputs.add(new JTextField(f.getName()));
				c.gridx=i;
				contentPane.add(inputs.get(i),c);
				i++;
			}
			
			c.gridx=2;
			c.gridy=5;
		
			submitOrder.addActionListener(this);
			contentPane.add(submitOrder,c);
			
			contentPane.revalidate();
			contentPane.repaint();
			
			pack();
			
			
			
		}
		if(arg0.getSource()==removeCustomer) 
		{
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(10,10,10,10);
			c.anchor=GridBagConstraints.CENTER;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 4;
			c.gridwidth = 2;

			clearBottom();
			inputs.add(new JTextField("enter id to delete"));
			contentPane.add(inputs.get(0),c);
			c.gridx=2;
			c.gridy=4;
			c.gridwidth=1;
			submitDelete.addActionListener(this);
			contentPane.add(submitDelete,c);
			
			contentPane.revalidate();
			contentPane.repaint();
			
			pack();
			
			
			
		}
		
		if(arg0.getSource()==submitDelete) 
		{
			String s=null;
			try {
			s=inputs.get(0).getText();
			}
			catch(IndexOutOfBoundsException e) 
			{
				
			}
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(10,10,10,10);
			c.anchor=GridBagConstraints.CENTER;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 4;
			c.gridwidth = 2;
			CustomerDAO delete=new CustomerDAO();
			int ok=2;
			if(s!=null)
			ok=delete.delete(Integer.parseInt(s));
			if(ok==0)
				JOptionPane.showMessageDialog(null, "Client deleted!");
			clearBottom();
			
						
			contentPane.revalidate();
			contentPane.repaint();
			
			pack();
			
			
			
		}
		
		
		if(arg0.getSource()==cancelOrder) 
		{
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(10,10,10,10);
			c.anchor=GridBagConstraints.CENTER;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 4;
			c.gridwidth = 2;

			clearBottom();
			inputs.add(new JTextField("enter id to cancel"));
			contentPane.add(inputs.get(0),c);
			c.gridx=2;
			c.gridy=4;
			c.gridwidth=1;
			cancelSubmit.addActionListener(this);
			contentPane.add(cancelSubmit,c);
			
			contentPane.revalidate();
			contentPane.repaint();
			
			pack();
			
			
			
		}
		
		if(arg0.getSource()==cancelSubmit) 
		{
			String s=null;
			try {
			s=inputs.get(0).getText();
			}
			catch(IndexOutOfBoundsException e) 
			{
				
			}
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(10,10,10,10);
			c.anchor=GridBagConstraints.CENTER;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 4;
			c.gridwidth = 2;
			OrderDAO delete=new OrderDAO();
			int ok=2;
			if(s!=null)
			ok=delete.delete(Integer.parseInt(s));
			if(ok==0)
				JOptionPane.showMessageDialog(null, "Order canceled!");
			clearBottom();
			
						
			contentPane.revalidate();
			contentPane.repaint();
			
			pack();
			
			
			
		}
		
		if(arg0.getSource()==removeProduct) 
		{
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(10,10,10,10);
			c.anchor=GridBagConstraints.CENTER;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 4;
			c.gridwidth = 2;

			clearBottom();
			inputs.add(new JTextField("enter id to delete"));
			contentPane.add(inputs.get(0),c);
			c.gridx=2;
			c.gridy=4;
			c.gridwidth=1;
			submitDeleteProduct.addActionListener(this);
			contentPane.add(submitDeleteProduct,c);
			
			contentPane.revalidate();
			contentPane.repaint();
			
			pack();
			
			
			
		}
		
		if(arg0.getSource()==submitDeleteProduct) 
		{
			String s=null;
			try {
			s=inputs.get(0).getText();
			}
			catch(IndexOutOfBoundsException e) 
			{
				
			}
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(10,10,10,10);
			c.anchor=GridBagConstraints.CENTER;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = 4;
			c.gridwidth = 2;
			ProductDAO delete=new ProductDAO();
			int ok=2;
			if(s!=null)
			ok=delete.delete(Integer.parseInt(s));
			if(ok==0)
				JOptionPane.showMessageDialog(null, "Product deleted!");
			clearBottom();
			
						
			contentPane.revalidate();
			contentPane.repaint();
			
			pack();
			
			
			
		}
		
		
		if(arg0.getSource()==submit) 
		{
			
			String[] s=new String[5];
			
			for(int i=0;i<inputs.size();i++) {
			s[i]=inputs.get(i).getText();
			
			
			}
		
			clearBottom();
			CustomerDAO insert1=new CustomerDAO();
			int ok=2;
			try {
			ok=insert1.insert(new Customer(Integer.parseInt(s[0]),s[1],s[2],s[3],s[4]));
			}
			catch(NumberFormatException e) 
			{
				
			}
			if(ok==0)
			JOptionPane.showMessageDialog(null, "The client has been added!");
			contentPane.revalidate();
			contentPane.repaint();
			pack();
		}
		if(arg0.getSource()==submitProduct) 
		{
			
			String[] s=new String[5];
			
			for(int i=0;i<inputs.size();i++) {
			s[i]=inputs.get(i).getText();
			
			
			}
			clearBottom();
			
			ProductDAO insert1=new ProductDAO();
			int ok=2;
			try {
			ok=insert1.insert(new Product(Integer.parseInt(s[0]),s[1],Integer.parseInt(s[2]),Integer.parseInt(s[3])));
			}
			catch(NumberFormatException e) 
			{
				
			}
			
			if(ok==0)
			JOptionPane.showMessageDialog(null, "The product has been added!");
			
			contentPane.revalidate();
			contentPane.repaint();
			pack();
		}
		
		if(arg0.getSource()==submitOrder) 
		{
			
			String[] s=new String[5];
			for(int i=0;i<inputs.size();i++) {
				s[i]=inputs.get(i).getText();
			}
			clearBottom();
			OrderDAO insert1=new OrderDAO();
			ProductDAO updateProd=new ProductDAO();
	
			System.out.println(s[2]+" and "+s[1]);
			try {
			int ok1=3;
			if(s[2]!=null && s[1]!=null)
			ok1=Validator.validateOrder(Integer.parseInt(s[2]), Integer.parseInt(s[1]),Integer.parseInt(s[3]));
			int ok=3;
			Product up = null;
		
			if(s[2]!=null && s[1]!=null) {
			up=updateProd.findById(Integer.parseInt(s[2]));
			}
			if(ok1==2)
			{
				try {
				ok=insert1.insert(new Order(Integer.parseInt(s[0]),Integer.parseInt(s[1]),Integer.parseInt(s[2]),Integer.parseInt(s[3])));
			
			 up.updateStock(Integer.parseInt(s[3]));
			 updateProd.update(up);}
				catch(NumberFormatException e) 
				{
						
				}
			}
			else
			if(ok1==1)
			{
				JOptionPane.showMessageDialog(null, "Not enough products on stock!");
				
			}
			else if(ok1==0)JOptionPane.showMessageDialog(null, "Invalid Order!");
			
			if(ok==0)
			JOptionPane.showMessageDialog(null, "The order has been submitted!");
				
			    
			
			}
			catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			contentPane.revalidate();
			contentPane.repaint();
			pack();
		}
		
		
	}

}
