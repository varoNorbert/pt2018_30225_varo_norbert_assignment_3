package model;

public class Order {
	public int idOrder;
	public int idCustomer;
	public int idProduct;
	public int quantity;

	
	public Order() {
		
	}
	public Order(int id1,int id2,int id3,int quantity) {
		
		this.setIdOrder(id1);
		this.setIdCustomer(id2);
		this.setIdProduct(id3);
		this.setQuantity(quantity);
	
		
	}
	public int getIdOrder() {
		return idOrder;
	}
	public void setIdOrder(int idOrder) {
		this.idOrder = idOrder;
	}
	public int getIdCustomer() {
		return idCustomer;
	}
	public void setIdCustomer(int idCustomer) {
		this.idCustomer = idCustomer;
	}
	public int getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
}
