package bll;

import DAO.CustomerDAO;
import DAO.ProductDAO;
import model.Product;

public class Validator {

	public static int validateOrder(int idProduct,int idCustomer,int quantity) 
	{
		
		CustomerDAO cust=new CustomerDAO();
		ProductDAO prod=new ProductDAO();
		Product toValidate=prod.findById(idProduct);
		if(cust.findById(idCustomer)!=null)
			if(toValidate!=null)
				if(toValidate.getStock()>quantity)
				return 2;
				else
				return 1;
		
		return 0;
		
	}
	
}
