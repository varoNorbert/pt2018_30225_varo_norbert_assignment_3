package DAO;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List; 

 

import java.util.logging.Logger;

import javax.swing.JOptionPane;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import connection.ConnectionDb;

public abstract class AbstractDAO<T> {
	
	protected static final Logger LOGGER=Logger.getLogger(AbstractDAO.class.getName());
	
	protected final Class<T> clazz; 
	

	public AbstractDAO() 
	{
		 clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]; 
	}
	
	private String buildSelectQuery(String s) 
	{
		StringBuilder query=new StringBuilder();
		query.append("Select ");
		query.append("* ");
		query.append(" from ");
		query.append(clazz.getSimpleName());
		query.append(" Where "+s+"=?");
		//System.out.println(query.toString());
		return query.toString();
	}
	private String buildInsertQuery(ArrayList<String> s1,ArrayList<String> s) 
	{
		StringBuilder query=new StringBuilder();
		query.append("insert into orders.");
		query.append(clazz.getSimpleName().toLowerCase());
		query.append(" (");
		for(int i=0;i<s1.size()-1;i++) 
		{
			query.append(s1.get(i)+",");
		}
		query.append(s1.get(s1.size()-1));
		query.append(")");
		query.append(" values (");
		for(int i=0;i<s.size()-1;i++) 
		{
			query.append("'"+s.get(i)+"'"+",");
		}
		query.append("'"+s.get(s.size()-1)+"'");
		query.append(")");
		
		//System.out.println(query.toString());
		return query.toString();
	}
	
	
	private String buildUpdateQuery(ArrayList<String> s1,ArrayList<String> s) 
	{
		StringBuilder query=new StringBuilder();
		query.append("update orders.`");
		query.append(clazz.getSimpleName().toLowerCase());
		query.append("` set ");
		for(int i=1;i<s1.size()-1;i++) 
		{
			query.append(s1.get(i)+"='"+s.get(i)+"', ");
		}		
		query.append(s1.get(s1.size()-1)+"='"+s.get(s.size()-1)+"' ");
		query.append("where "+s1.get(0)+"="+s.get(0));
		
		
		System.out.println(query.toString());
		return query.toString();
	}
	
	
	public List<T> returnAll() 
	{
		Connection con=ConnectionDb.getConnection();
		PreparedStatement ps=null;
		ResultSet rs=null;
		try {
			Field[] field=clazz.getDeclaredFields();
			String query="select * from "+ "`"+clazz.getSimpleName()+"`";
			System.out.println(query);
			try {
				ps=(PreparedStatement) con.prepareStatement(query);
				rs=ps.executeQuery();
				return createObjects(rs);
			
				
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally 
			{
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
			
			
			
			
			
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		 
		
		return null;
	}
	
	public int insert(T toInsert) 
	{
		Connection con=ConnectionDb.getConnection();
		PreparedStatement ps=null;
		try {
			Field[] field=clazz.getDeclaredFields();
			ArrayList<String> queryArgsVal=new ArrayList<String>();
			ArrayList<String> queryArgsCol=new ArrayList<String>();
			for(Field f:field) 
			{
				queryArgsCol.add(f.getName());
			}
			int i=0;
			for(Field f:field) 
			{
				if(i==0) 
				{	i++;
					String q="Select * from `"+clazz.getSimpleName()+"` where "+ f.getName() +"="+Integer.toString((int)f.get(toInsert));
					System.out.println(q);
					PreparedStatement checkId=(PreparedStatement) con.prepareStatement(q);
					ResultSet rs=checkId.executeQuery();
					if(rs.next()) 
					{
						JOptionPane.showMessageDialog(null,"This ID already exists!");
						return 1;
					}
				}
				Object value=f.get(toInsert);
				if(value.getClass()==String.class) 
				{
					queryArgsVal.add((String) value);
				}
				else 
				{
					int a=(int)value;
					queryArgsVal.add(Integer.toString(a));
				}
				
			}
			
			
			String query=buildInsertQuery(queryArgsCol,queryArgsVal);
			System.out.println(query);
			try {
				ps=(PreparedStatement) con.prepareStatement(query);
				ps.executeUpdate(query);
				
			
				
				
				
			}
			catch (SecurityException e) { } catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally 
			{
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
			
			
			
			
			
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalArgumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
		
	}
	
	
	public int update(T toUpdate) 
	{
		Connection con=ConnectionDb.getConnection();
		PreparedStatement ps=null;
		try {
			Field[] field=clazz.getDeclaredFields();
			ArrayList<String> queryArgsVal=new ArrayList<String>();
			ArrayList<String> queryArgsCol=new ArrayList<String>();
			for(Field f:field) 
			{
				queryArgsCol.add(f.getName());
			}
			int i=0;
			for(Field f:field) 
			{
				if(i==0) 
				{	i++;
					String q="Select * from `"+clazz.getSimpleName()+"` where "+ f.getName() +"="+Integer.toString((int)f.get(toUpdate));
					System.out.println(q);
					PreparedStatement checkId=(PreparedStatement) con.prepareStatement(q);
					ResultSet rs=checkId.executeQuery();
					if(!rs.next()) 
					{
						JOptionPane.showMessageDialog(null,"This ID does not exist!");
						return 1;
					}
				}
				
				Object value=f.get(toUpdate);
				if(value.getClass()==String.class) 
				{
					queryArgsVal.add((String) value);
				}
				else 
				{
					int a=(int)value;
					queryArgsVal.add(Integer.toString(a));
				}
				
				
			}
			
			
			String query=buildUpdateQuery(queryArgsCol,queryArgsVal);
			System.out.println(query);
			try {
				ps=(PreparedStatement) con.prepareStatement(query);
				ps.executeUpdate(query);
				
			
				
				
				
			}
			catch (SecurityException e) { } catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally 
			{
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
			
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalArgumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
		
	}
	
	
	
	
	public int delete(int id) 
	{
		Connection con=ConnectionDb.getConnection();
		PreparedStatement ps=null;
		try {
			
		
					Field[] fields=clazz.getDeclaredFields();
					
					String q="Select * from `"+clazz.getSimpleName()+"` where "+fields[0].getName() +"="+id;
					System.out.println(q);
					PreparedStatement checkId=(PreparedStatement) con.prepareStatement(q);
					ResultSet rs=checkId.executeQuery();
					if(!rs.next()) 
					{
						JOptionPane.showMessageDialog(null,"This ID doesn't exist!");
						return 1;
					}
				
				String deleteQuery="delete from `"+clazz.getSimpleName()+"` where "+fields[0].getName() +"="+id;
				
				
			try {
				ps=(PreparedStatement) con.prepareStatement(deleteQuery);
				ps.executeUpdate(deleteQuery);
				
			
				
				
				
			}
			catch (SecurityException e) { } catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally 
			{
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
			
			
			
			
			
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalArgumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
		
	}
	
	
	public T findById(int id) 
	{
		
		//Customer customer=null;
		Connection con=ConnectionDb.getConnection();
		PreparedStatement ps=null;
		ResultSet rs=null;
		try {
			Field[] field=clazz.getDeclaredFields();
			String query=buildSelectQuery(field[0].getName());
			try {
				ps=(PreparedStatement) con.prepareStatement(query);
				ps.setInt(1,id);
				rs=ps.executeQuery();
				if(rs.isBeforeFirst())
				return createObjects(rs).get(0);
				else return null;
				
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally 
			{
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
			
			
			
			
			
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		 
		
		return null;
		
	}

	private List<T> createObjects(ResultSet rs) {
		List<T> list= new ArrayList<T>();
		
		try {
			while(rs.next()) 
			{
				try {
					T instance=clazz.newInstance();
					for(Field field:clazz.getDeclaredFields()) 
					{
						//System.out.println(clazz.getSimpleName());
						Object value=rs.getObject(field.getName());
						PropertyDescriptor propertyDescriptor= new PropertyDescriptor(field.getName(),clazz);
						Method method=propertyDescriptor.getWriteMethod();
						method.invoke(instance,value);
					}
					list.add(instance);
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IntrospectionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
}
